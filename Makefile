SRC	=	src/minishell.c			\
		src/executer.c			\
		src/repeat.c			\
		src/list.c			\
		src/which.c			\
		src/gnl.c			\
		src/str_to_tab.c		\
		src/my_strcpy.c			\
		src/str_to_wordlist.c		\
		src/strdup.c			\
		src/strcat.c			\
		src/strncmp.c			\
		src/setenv_func.c		\
		src/unsetenv_func.c		\
		src/cd_func.c			\
		src/tools_bis.c			\
		src/where.c			\
		src/tools_ter.c			\
		src/env_print.c			\
		src/getnextline.c		\
		src/semi.c			\
		src/error.c			\
		src/tools.c			\
		src/redirection.c		\
		src/pipe2.c			\
		src/globbing.c			\
		src/echo.c			\
		src/pipe.c			\
		src/itoa.c

OBJ	=	$(SRC:.c=.o)

NAME	=	42sh

CC	=	gcc -Wall -Wextra -Werror -g3

all:	$(NAME)

$(NAME):	$(OBJ)
	cd ./lib/my_printf ; make
	$(CC) -o $(NAME) $(SRC) ./lib/libmy_printf.a

valgrind:	$(OBJ)
	cd ./lib/my_printf ; make
	$(CC) -g3 -o $(NAME) $(SRC) ./lib/libmy_printf.a

clean:
	cd ./lib/my_printf ; make clean
	rm -f $(OBJ)

fclean:	clean
	cd ./lib/my_printf ; make fclean ; cd .. ; rm libmy_printf.a
	rm -f $(NAME)

re:	fclean all

