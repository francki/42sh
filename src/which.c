#include "../include/minishell.h"

int	acces(list *s, char *str)
{
	while (s) {
		if (access(s->path, F_OK) == 0) {
			my_printf("%s\n", s->path);
			return (0);
		}
		s = s->next;
	}
	my_printf("%s: Command not found.\n", str);
	return (1);
}

int	check_builtins(char *str)
{
	int b = 0;
	(my_strcmp(str, "echo") == 0) ?
		my_printf("%s: shell built-in command.\n",str), b += 1 : 0;
	(my_strcmp(str, "repeat") == 0) ?
		my_printf("%s: shell built-in command.\n",str), b += 1 : 0;
	(my_strcmp(str, "setenv") == 0) ?
		my_printf("%s: shell built-in command.\n",str), b += 1 : 0;
	(my_strcmp(str, "unsetenv") == 0) ?
		my_printf("%s: shell built-in command.\n",str), b += 1 : 0;
	(my_strcmp(str, "cd") == 0) ?
		my_printf("%s: shell built-in command.\n",str), b += 1 : 0;
	(my_strcmp(str, "where") == 0) ?
		my_printf("%s: shell built-in command.\n",str), b += 1 : 0;
	(my_strcmp(str, "which") == 0) ?
		my_printf("%s: shell built-in command.\n",str), b += 1 : 0;
	if (b > 0)
		return (1);
	else
		return (0);
}

int	which(char **argv, char **env, int *ret)
{
	int i = 1;
	list *path = get_path(tabdup(env));

	while (argv[i]) {
		path_concatener(path, argv[i]);
		if (check_builtins(argv[i]) == 0)
			*ret = acces(path, argv[i]);
		i++;
	}
	return (0);
}
