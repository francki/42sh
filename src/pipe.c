#include "../include/minishell.h"

int	execute_pipe(pipe_s s, char *swap)
{
	int pid = fork();
	int status;
	int ex = 0;

	errno = 0;
	if (!pid) {
		if (dupper(s, &ex, swap) == 84)
			return (84);
	}
	if (ex == -1)
		status = error_exe(s.argv[0]);
	waitpid(pid, &status, 0);
	error_hl(status);
	ex != -1 ? 0 : exit(status);
	return (return_selector(status));
}

int	launch_pipe(list *tmp, list *path, char ***env, char *sw)
{
	char *gp = NULL;
	char *dest = epure_red(tmp->str);
	char **argv = str_to_tab(dest, ' ');
	int ret = 0;

	path_concatener(path, argv[0]);
	if (hub(argv, env, &ret))
		return (ret);
	else if ((gp = check_access(path, argv[0]))) {
		return (execute_pipe(init_s(gp, argv, *env, tmp), sw));
	} else
		my_printf("%s: Command not found.\n", argv[0]);
	return (1);
}

int	pipe_cmd(char *str, list *path, char ***env, int *ret)
{
	list *full = NULL;
	list *tmp = NULL;
	char sw = 1;

	str_to_wordlist(str, &full, '|');
	tmp = full;
	while (tmp) {
		tmp->str = my_epure_strr(tmp->str);
		*ret = launch_pipe(tmp, path, env, &sw);
		tmp = tmp->next;
		sw = (~sw & 1) | (sw & ~1);
		parse_and_x(my_strcat("rm ../.tmp", my_itoa(sw)), env, ret);
	}
	parse_and_x(my_strcat("rm ../.tmp", my_itoa(sw ? 0 : 1)), env, ret);
	return (0);
}
