#include "../include/minishell.h"

int	intlen(int num)
{
	int i = 0;

	if (num == 0)
		return (1);
	while (num > 0) {
		num /= 10;
		i++;
	}
	return (i);
}

int	ten_exp(int num, int exp)
{
	return (exp - 1 ? ten_exp(num * 10, exp - 1) : num);
}

char	*my_itoa(int num)
{
	int i = 0;
	int modulo = ten_exp(1, intlen(num));
	char *ret = malloc(sizeof(char) * intlen(num) + 1);

	while (modulo) {
		ret[i] = num / modulo + '0';
		num %= modulo;
		modulo /= 10;
		i++;
	}
	ret[i] = '\0';
	return (ret);
}
