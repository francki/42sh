#include "../include/minishell.h"

int	my_strcmp(char *str1, char *str2)
{
	int i = -1;

	while (str1[++i] && str2[i])
		if (str1[i] != str2[i])
			return (str1[i] - str2[i]);
	if (my_strlen(str1) != my_strlen(str2))
		return (-1);
	return (0);
}

int	my_strncmp(char *str1, char *str2, int n)
{
	int i = -1;

	while (str1[++i] && str2[i] && i <= n)
		if (str1[i] != str2[i])
			return (str1[i] - str2[i]);
	return (0);
}
