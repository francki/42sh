#include "../include/minishell.h"

char	*redirection(char *str)
{
	int i = 0;
	int a = 0;
	char *dest = malloc(sizeof(char)*my_strlen(str));

	while (str[i]) {
		if ((str[i] == '>' && str[i+1] == '>') ||
		(str[i] == '<' && str[i+1] == '<'))
			i++;
		else {
			dest[a] = str[i];
			a++;
			i++;
		}
	}
	dest[a] = '\0';
	return (dest);
}

void	bwhile(char *file)
{
	while (my_strcmp(get_next_line(0), file) != 0)
		my_printf("? ");
}

char	*recup_file(char *str)
{
	int i = 0;
	int a = 0;
	char *dest = malloc(sizeof(char) * my_strlen(str) + 1);

	if (!dest)
		return (NULL);
	while (str[i-1] != '>' && str[i-1] != '<' && str[i]) {
		i++;
	}
	if (str[i] == ' ')
		i++;
	while (str[i]) {
		dest[a] = str[i];
		a++;
		i++;
	}
	dest[a] = '\0';
	return (dest);
}

int	redirection_rev(char *str, char *check)
{
	int flags;
	int fd;
	char *file = recup_file(check);

	if (cmpt_tab(str_to_tab(check, '<')) == 3) {
		flags = O_CREAT | O_RDWR;
		if (my_strcmp(check, str) != 0) {
			my_printf("? ");
			bwhile(file);
			my_printf("\0");
		} else {
			fd = open(file, flags, 0644);
			dup2(fd, 0);
		}
		if (fd == -1)
			return (84);
	}
	return (0);
}

int	redirection_dup(char *str)
{
	char *check;
	int flags;
	char *file;
	int fd;

	check = redirection(str);
	file = recup_file(check);
	if (cmpt_tab(str_to_tab(check, '>')) == 3) {
		flags = O_CREAT | O_RDWR;
		if (my_strcmp(check, str) != 0) {
			fd = open(file, flags | O_APPEND, 0644);
		} else
			fd = open(file,	flags, 0644);
		if (fd == -1)
			return (84);
		dup2(fd, 1);
	}
	redirection_rev(str, check);
	return (0);
}
