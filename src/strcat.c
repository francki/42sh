#include "../include/minishell.h"

char	*my_strcat(char *buf, char *str)
{
	int i = 0;
	int j = 0;
	char *ret;

	if (!str || !(ret = malloc((buf ? my_strlen(buf) : 0) +
	(str ? my_strlen(str) : 0) + 1)))
		return (buf);
	while (buf && buf[i]) {
		ret[i] = buf[i];
		++i;
	}
	while (str && str[j]) {
		ret[i + j] = str[j];
		++j;
	}
	ret[i + j] = '\0';
	return (ret);
}
