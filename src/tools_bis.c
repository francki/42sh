#include "../include/minishell.h"

char	*get_pwd(void)
{
	char *buf = 0;

	return (getcwd(buf, MAX_PATH));
}

char	*env_search(char *str, int n, char ***env)
{
	char **ptr = *env;

	while (*ptr) {
		if (!(my_strncmp(str, *ptr, n)))
			return (*ptr + n);
		(ptr)++;
	}
	return (NULL);
}

char	**argv_maker(char *str, char *str2, char *str3)
{
	char **ret = malloc(sizeof(char *) * 4);

	ret[0] = my_strdup(str);
	ret[1] = my_strdup(str2);
	ret[2] = my_strdup(str3);
	ret[3] = NULL;
	return (ret);
}

void	free_tab(char **tab)
{
	int i = -1;

	while (tab[++i])
		free(tab[i]);
	free(tab);
}

int	cmpt_tab(char **tab)
{
	int i = 0;

	while (tab[i++]);
	return (i);
}
