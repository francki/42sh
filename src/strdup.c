#include "../include/minishell.h"

char	*my_strdup(char *str)
{
	int i = -1;
	char *ret = malloc(sizeof(char) * my_strlen(str) + 1);

	while (str[++i])
		ret[i] = str[i];
	ret[i] = '\0';
	return (ret);
}
