#include "../include/minishell.h"

char	**tabdup(char **tab)
{
	int i = -1;
	char **ret = malloc(sizeof(char *) * (my_tablen(tab) + 1));

	if (!ret)
		return (NULL);
	if (!tab[0])
		return (ret);
	while (tab[++i])
		ret[i] = my_strdup(tab[i]);
	ret[i] = NULL;
	return (ret);
}
