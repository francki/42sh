#include "../include/minishell.h"
#include <stdio.h>

char *str_argv(char **argv)
{
	int i = -1;
	char *str = "";
	while (argv[++i]) {
		str = my_strcat(str, argv[i]);
		str = my_strcat(str, " ");
	}
	return (str);
}

int repeat_error(char **argv)
{
	int nb = 0; int i = -1; int s = 1;
	while (argv[++i]);
	if (i < 3) {
		my_printf("repeat: Too few arguments\n");
		return (-1);
	}
	i = -1;
	if (argv[1][0] == '-') {
		argv[1] = &argv[1][1];
		s = -1;
	}
	while (argv[1][++i])
		if (argv[1][i] < '0' || argv[1][i] > '9') {
			my_printf("repeat: Badly formed number.\n");
			return (-1);
		}
	nb = atoi(argv[1]) * s;
	if (nb <= 0)
		return (-1);
	return (nb);
}

char *repeat_func(char **argv, char ***env, int *ret)
{
	char *str = NULL;
	char *tmp = NULL;
	int i = 0;
	int  nb = 0;

	if ((nb = repeat_error(argv)) == -1) {
		*ret = 1;
		return (NULL);
	}
	str = str_argv(&argv[2]);
	tmp = my_strdup(str);
	while (++i != nb) {
		str = my_strcat(str, ";");
		str = my_strcat(str, " ");
		str = my_strcat(str, tmp);
	}
	parse_and_x(str, env, ret);
	return (str);
}
