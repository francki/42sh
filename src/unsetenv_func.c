#include "../include/minishell.h"

int	unsetenv_err(char **argv)
{
	int i = -1;

	while (argv[++i]);
	if (i == 1) {
		my_printf("unsetenv: Too few arguments.\n");
		return (1);
	}
	return (0);
}

int	in_env(char **argv, char *token)
{
	int i = -1;

	while (argv[++i])
		if (my_strncmp(argv[i], token, my_strlen(argv[i])) == 0)
			return (1);
	return (0);
}

char	**make_unsetenv_func(char **argv, char ***env)
{
	char **ret = malloc(sizeof(char *) * my_tablen(*env));
	int i = -1;
	int j = 0;

	if (unsetenv_err(argv)) {
		free_tab(ret);
		return (NULL);
	}
	while ((*env)[++i] != NULL) {
		if (!in_env(argv, (*env)[i])) {
			ret[j] = (*env)[i];
			j++;
		}
	}
	ret[j] = NULL;
	free(*env);
	return (ret);
}

void	unsetenv_func(char **argv, char ***env)
{
	*env = make_unsetenv_func(argv, env);
}
