#include "../include/minishell.h"

int	char_oc(char *str, char c)
{
	int i = -1;
	int count = 1;

	while (str[++i])
		if (str[i] == c)
			count++;
	return (count);
}

char	**str_to_tab(char *str, char c)
{
	int i = -1;
	int j = 0;
	int k = 0;
	char **ret = malloc(sizeof(char*) * (char_oc(str, c) + 2));
	char *tmp = my_strdup(str);

	while (tmp[++i]) {
		if (tmp[i] == c) {
			tmp[i] = '\0';
			ret[j] = my_strdup(&tmp[k]);
			j++;
			k = i + 1;
		}
	}
	ret[j] = my_strdup(&tmp[k]);
	ret[j + 1] = NULL;
	return (ret);
}
