#include "../include/minishell.h"

void	handling_c(int signum)
{
	if (signum == SIGINT)
		my_printf("\n$>");
}

int	error_exe(char *argv)
{
	write(2, argv, my_strlen(argv));
	if (errno == ENOEXEC)
		write(2, ": Exec format error. Wrong Architecture.\n", 41);
	else if (errno == EACCES)
		write(2, ": Permission denied.\n", 21);
	return (1);
}

void	error_hl(int status)
{
	char b = 0;

	WTERMSIG(status) == 11 ?
	write(2, "Segmentation fault", 18), (b = 1) : 0;
	WTERMSIG(status) == 8 ? write(2, "Floating exception", 18), (b = 1) : 0;
	if (WCOREDUMP(status))
		write(2, " (core dumped)", 14);
	b ? write(2, "\n", 1) : 0;
}
