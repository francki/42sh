#include "../include/minishell.h"

int	dupper(pipe_s s, int *ex, char *swap)
{
	int tmp0;
	int tmp1;

	redirection_dup(s.t->str);
	if ((tmp0 = opener_pipe(0)) == -1)
		return (84);
	if ((tmp1 = opener_pipe(1)) == -1)
		return (84);
	if (s.t->prev)
		dup2(*swap ? tmp0 : tmp1, 0);
	if (s.t->next)
		dup2(*swap ? tmp1 : tmp0, 1);
	*ex = execve(s.s, s.argv, s.env);
	close(tmp0);
	close(tmp1);
	return (0);
}

int	opener_pipe(char sw)
{
	int fd = open(my_strcat("../.tmp", my_itoa(sw)),
	O_CREAT | O_RDWR, 0644);

	return (fd);
}

pipe_s	init_s(char *s, char **argv, char **env, list *t)
{
	pipe_s ret;

	ret.s = s;
	ret.argv = argv;
	ret.env = env;
	ret.t = t;
	return (ret);
}

int	check_pipe(char *str)
{
	int i = 0;

	while (str[i]) {
		if (str[i] == '|')
			return (0);
		i++;
	}
	return (1);
}
