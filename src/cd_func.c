#include "../include/minishell.h"

char	*old_p(int i)
{
	static char *old = NULL;
	if (i == 0)
		old = getcwd(old, 256);
	return (old);
}

int	cd_minus(void)
{
	char *str = NULL;
	if (old_p(1) == NULL) {
		my_printf(": No such file or directory.\n");
		return (1);
	} else {
		str = my_strdup(old_p(1));
		old_p(0);
		chdir(str);
	}
	return (0);
}

int	cd_classic(char ***env, char *pwd, char *str, char *first)
{
	char *ret = my_strcat(pwd, "/");
	char **old = argv_maker("setenv", "OLDPWD", pwd);
	*first = 0;
	old_p(0);
	ret = my_strcat(ret, str);
	if (access(ret, F_OK) == -1) {
		write(2, str, my_strlen(str));
		write(2, ": No such file or directory.\n", 29);
		return (1);
	}
	if (chdir(ret)) {
		write(2, str, my_strlen(str));
		write(2, ": Not a directory.\n", 19);
		setenv_func(old, env);
		return (1);
	}
	setenv_func(old, env);
	return (0);
}

void	cd_home(char ***env, char *pwd, char *first)
{
	char *home = env_search("HOME=", 5, env);
	char **argv = argv_maker("setenv", "OLDPWD", pwd);

	*first = 0;
	setenv_func(argv, env);
	old_p(0);
	chdir(home);
}

void	cd_func(char **argv, char ***env, int *ret)
{
	char *pwd = get_pwd();
	static char first = 1;

	if (!argv[1] || argv[1][0] == '~' || !my_strcmp(argv[1], "--"))
		cd_home(env, pwd, &first);
	else if (argv[1][0] == '-')
		*ret = cd_minus();
	else if (argv[1][0] == '/')
		*ret = cd_classic(env, "", argv[1], &first);
	else
		*ret = cd_classic(env, pwd, argv[1], &first);
}
