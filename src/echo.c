#include "../include/minishell.h"

void	echo(char **argv)
{
	int bool = 0; int i = 1; char *str;
	if (argv[1] != NULL && my_strcmp(argv[1], "-n") == 0) {
		bool = 1; i = 2;
	} else
		i = 1;
	if (is_viable(argv) == 2) {
		i++; my_printf("Unmatched '\"'.\n");
		return;
	}
	while (argv[i] != NULL) { str = argv[i];
		if (str[0] == '"' && str[my_strlen(str) - 1] == '"')
			echo_with(str);
		else
			echo_without(str);
		if (argv[i + 1] != NULL)
			my_printf(" ");
		i++;
	} if (bool == 0)
		my_printf("\n");
}

void	echo_without(char *str)
{
	int i = 0;

	while (str[i]) {
		(str[i] == '\\' && str[i + 1] != '\\') ? i++ :
		(str[i] == '\\' && str[i + 1] == '\\' && str[i + 2] == 'n') ?
		(my_printf("\n") , i += 3) :
		(str[i] == '\\' && str[i + 1] == '\\' && str[i + 2] == 't') ?
		(my_printf("\t") , i += 3) :
		(str[i] == '\\' && str[i + 1] == '\\' && str[i + 2] == 'b') ?
		(my_printf("\b") , i += 3) : (my_printf("%c", str[i]), i++);
	}
}

void	echo_with(char *str)
{
	int i = 1;

	while (i != my_strlen(str) - 1) {
		(str[i] == '\\' && str[i + 1] == 'n') ?
			write(1, "\n", 1), i = i + 1 :
		(str[i] == '\\' && str[i + 1] == 't') ?
			write(1, "\t", 1), i = i + 1 :
		(str[i] == '\\' && str[i + 1] == 'b') ?
			write(1, "\b", 1), i = i + 1 :
		my_printf("%c", str[i]);
		i++;
	}
}

int	is_viable(char **av)
{
	int i = -1;
	int j = 0;
	int k = -1;
	char *str;

	while (av[++k]) {
		i = -1;
		j = 0;
		str = av[k];
		while (str[++i]) {
			if (str[i] == '"')
				j++;
		}
		if (j % 2 == 0)
			continue;
		else
			return (2);
	}
	return (0);
}
