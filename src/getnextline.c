#include "../include/minishell.h"

int	my_strlen(char *str)
{
	int i = 0;

	while (str[i] != '\0') {
		i = i + 1;
	}
	return (i);
}

char	*get_next_line(int fd)
{
	char *buffer;
	char *save;
	int ret = 1;

	if ((save = malloc(sizeof(char) * 2)) == NULL ||
		(buffer = malloc(sizeof(char) * 2)) == NULL)
		return (NULL);
	while (ret == 1) {
		if ((ret = read(fd, buffer, 1)) == 0)
			return (NULL);
		buffer[ret] = '\0';
		if (buffer[0] == '\n')
			ret = 0;
		else
			save = my_strcat(save, buffer);
	}
	return (save);
}
