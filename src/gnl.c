#include "../include/minishell.h"

char	*my_gnl(const int fd)
{
	int		i;
	static int	size;
	static int	pos = -1;
	static char	buff[4096];
	char		*str;

	if ((fd < 0) || (pos == -1 && (size = read(fd, buff, 4096)) <= 0))
		return (NULL);
	i = pos;
	while (buff[++i] != '\0' && buff[i] != '\n');
	if ((str = malloc(sizeof(char) * (i - pos))) == NULL)
		return (NULL);
	i = -1;
	while (++pos < (size - 1) && buff[pos] != '\n')
		str[++i] = buff[pos];
	str[++i] = '\0';
	if (pos >= (size - 1))
		pos = -1;
	return (str);
}
