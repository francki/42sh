#include "../include/minishell.h"

int	tab_len(char **tab)
{
	int i = -1;

	while (tab[++i]);
	return (i);
}

char	*check_access(list *s, char *argv)
{
	int pid;

	while (s && (pid = access(s->path, F_OK)))
		s = s->next;
	if (s)
		return (s->path);
	else if (!(pid = access(argv, F_OK)))
		return (argv);
	return (NULL);
}

list	*get_path(char **env)
{
	list *paths = NULL;
	int i = 0;
	char *path = my_strdup("/usr/local/bin:/usr/bin:/bin");

	while (env[i] && my_strncmp(env[i], "PATH=", 5) != 0)
		i++;
	if (env[i] == NULL) {
		str_to_wordlist(path, &paths, ':');
		return (paths);
	}
	str_to_wordlist(env[i] + 5, &paths, ':');
	return (paths);
}

void	path_concatener(list *paths, char *str)
{
	if (my_strlen(paths->str) == 0 || str[0] == '/' ||
	my_strcmp(str, "bin/ls") == 0)
		return;
	while (paths) {
		paths->path = my_strcat(paths->str, "/");
		paths->path = my_strcat(paths->path, str);
		paths = paths->next;
	}
}

char	*my_epure_strr(char *str)
{
	char *ret = NULL;
	int i = -1;
	int j = 0;
	if (!str)
		return (NULL);
	else if (!(ret = malloc(sizeof(char) * (my_strlen(str) + 1))))
		return (NULL);
	while (str[++i] && ((str[i] == ' ') || (str[i] == '\t')));
	while (str[i]) {
		(str[i] == '`') ? i++ : 0;
		ret[j++] = str[i++];
		while ((str[i] == ' ') || (str[i] == '\t'))
			i++;
		if (((str[i - 1] == ' ') || (str[i - 1] == '\t')) &&
				(str[i]))
			ret[j++] = ' ';
	}
	ret[j] = '\0';
	free(str);
	return (ret);
}
