#include "../include/minishell.h"

void	env_print(char **env)
{
	while (*env)
		my_printf("%s\n", *env++);
}
