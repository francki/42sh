#include "../include/minishell.h"

int	return_selector(int status)
{
	if (status == 11)
		return (139);
	else if (status == 8)
		return (136);
	if (status == 256 || status == 512)
		return (1);
	return (status);
}

int	hub(char **argv, char ***env, int *ret)
{
	char b = 0;

	if (argv[0][0] == 0)
		return (1);
	my_strcmp(argv[0], "which") ? 0 : (which(argv, *env, ret), (b += 1));
	my_strcmp(argv[0], "where") ? 0 : (where(argv, *env, ret), (b += 1));
	my_strcmp(argv[0], "env") ? 0 : (env_print(*env), (b += 1));
	my_strcmp(argv[0], "echo") ? 0 : (echo(argv), (b += 1));
	my_strcmp(argv[0], "repeat") ? 0 : (repeat_func(argv, env, ret),
	(b += 1));
	my_strcmp(argv[0], "setenv") ? 0 : (setenv_func(argv, env),
	(b += 1));
	my_strcmp(argv[0], "unsetenv") ? 0 : (unsetenv_func(argv, env),
	(b += 1));
	my_strcmp(argv[0], "cd") ? 0 : (cd_func(argv, env, ret), (b += 1));
	return (b);
}

int	execute(char *s, char **argv, char **env, char *str)
{
	int pid = fork();
	int status;
	int ex = 0;

	errno = 0;
	if (!pid) {
		redirection_dup(str);
		ex = execve(s, argv, env);
	}
	if (ex == -1)
		status = error_exe(argv[0]);
	waitpid(pid, &status, 0);
	error_hl(status);
	ex != -1 ? 0 : exit(status);
	return (return_selector(status));
}
