#include "../include/minishell.h"

char	*epure_red(char *str)
{
	int i = 0;
	char *dest = malloc(sizeof(char) * my_strlen(str) + 1);

	if (!dest)
		return (NULL);
	while (str[i]) {
		if (str[i] == '>' || str[i] == '<' ||
		(str[i] == ' ' && str[i + 1] == '>') ||
		(str[i] == ' ' && str[i + 1] == '<')) {
			dest[i] = '\0';
			return (dest);
		}
		dest[i] = str[i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}

int	launch(char *str, list *path, char ***env)
{
	char *good_path = NULL;
	char *dest = epure_red(str);
	char **argv = str_to_tab(dest, ' ');
	int ret = 0;

	path_concatener(path, argv[0]);
	if (hub(argv, env, &ret))
		return (ret);
	else if ((good_path = check_access(path, argv[0])))
		return (execute(good_path, argv, *env, str));
	else
		my_printf("%s: Command not found.\n", argv[0]);
	return (1);
}

int	minishell(char **env)
{
	char *str = NULL;
	char *prompt = "$>";
	int ret;
	char **env_cp = tabdup(env);

	while (1) {
		write(0, prompt, my_strlen(prompt));
		str = my_gnl(0);
		if (!str || my_strcmp(str, "exit") == 0) {
			isatty(0) ? my_printf("exit\n") : 0;
			return (ret);
		}
		if ((str = globbing(str)) != NULL)
			parse_and_x(str, &env_cp, &ret);
		else
			ret = 1;
	}
	return (ret);
}

int	main(__attribute__((unused))int ac, __attribute__((unused))
		char **argv, char **env)
{
	signal(SIGINT, handling_c);
	return (minishell(env));
}
