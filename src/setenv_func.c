#include "../include/minishell.h"

int	my_tablen(char **str)
{
	int i = -1;

	if (!str)
		return (1);
	while (str[++i]);
	return (i);
}

int	check_alphanumeric(char *str)
{
	int i = 0;

	while (str[i]) {
		if ((str[i] >= 48 && str[i] <= 57) ||
		(str[i] >= 65 && str[i] <= 90) ||
		(str[i] >= 97 && str[i] <= 122))
			i++;
		else
			return (1);
	}
	return (0);
}

int	setenv_error_check(char **argv, char **env)
{
	int i = -1;

	while (argv[++i]);
	if (i > 3)
		write(2, "setenv: Too many arguments.\n", 28);
	else if (i == 1)
		env_print(env);
	else if (check_alphanumeric(argv[1]) == 1) {
		write(2, "setenv: Variable name must", 26);
		write(2, " contain alphanumeric characters.\n", 34);
		return (4);
	}
	return (i);
}

char	**my_setenv(char **argv, char **env)
{
	char **ret = malloc(sizeof(char *) * (my_tablen(env) + 2));
	int i = -1;

	if (!ret)
		return (NULL);
	while (env[++i])
		ret[i] = my_strdup(env[i]);
	ret[i] = my_strcat(argv[1], "=");
	ret[i] = my_strcat(ret[i], argv[2]);
	ret[i + 1] = NULL;
	return (ret);
}

void	setenv_func(char **argv, char ***env)
{
	char **ptr = *env;
	char b = 0;
	int ret;

	if ((ret = setenv_error_check(argv, *env)) > 3 || ret == 1)
		return;
	while (*ptr) {
		if (!my_strncmp(*ptr, argv[1], my_strlen(argv[1]))) {
			*ptr = my_strcat(argv[1], "=");
			*ptr = my_strcat(*ptr, argv[2]);
			b = 1;
		}
		ptr++;
	}
	b ? 0 : (*env = my_setenv(argv, *env));
}
