#include "../include/minishell.h"

void	str_to_wordlist(char *str, list **head, char c)
{
	int i = -1;
	char b = 0;

	while (str[++i]) {
		if (str[i] == c) {
			str[i] = '\0';
			append_string(head, str);
			str_to_wordlist(&str[i + 1], head, c);
			i--;
			b = 1;
		}
	}
	b ? 0 : append_string(head, str), (b = 1);
}
