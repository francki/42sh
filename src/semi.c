#include "../include/minishell.h"

char	*trans_string(char *str)
{
	int i;
	int a;
	char *dest = malloc(sizeof(char) * my_strlen(str) + 1);

	if (!dest)
		return (NULL);
	for (i = 0, a = 0 ; str[i] ; a++, i++) {
		if ((str[i] == '&' && str[i + 1] == '&') ||
		(str[i] == '|' && str[i + 1] == '|')) {
			i += 1;
		}
		dest[a] = str[i];
	}
	dest[a] = '\0';
	return (dest);
}

int	check_and_or(char *str)
{
	int i = 0;

	while (str[i]) {
		if (str[i] == '&' && str[i + 1] == '&')
			return (2);
		if (str[i] == '|' && str[i + 1] == '|')
			return (1);
		i++;
	}
	return (0);
}

int	and(char *str, list *path, char ***env, int *ret)
{
	list *full = NULL;
	list *tmp = NULL;

	str = trans_string(str);
	str_to_wordlist(str, &full, '&');
	tmp = full;
	while (tmp) {
		tmp->str = my_epure_strr(tmp->str);
		*ret = launch(tmp->str, path, env);
		if (*ret != 0)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

int	or(char *str, list *path, char ***env, int *ret)
{
	list *full = NULL;
	list *tmp = NULL;

	str = trans_string(str);
	str_to_wordlist(str, &full, '|');
	tmp = full;
	while (tmp) {
		tmp->str = my_epure_strr(tmp->str);
		*ret = launch(tmp->str, path, env);
		if (*ret == 0)
			return (1);
		tmp = tmp->next;
	}
	return (0);
}

void	parse_and_x(char *str, char ***env, int *ret)
{
	list *full = NULL;
	list *tmp = NULL;
	list *new_path = get_path(tabdup(*env));

	str_to_wordlist(str, &full, ';');
	tmp = full;
	while (tmp) {
		tmp->str = my_epure_strr(tmp->str);
		if (check_and_or(tmp->str) == 2)
			and(tmp->str, new_path, env, ret);
		else if (check_and_or(tmp->str) == 1)
			or(tmp->str, new_path, env, ret);
		else if (check_pipe(tmp->str) == 0)
			pipe_cmd(tmp->str, new_path, env, ret);
		else
			*ret = launch(tmp->str, new_path, env);
		tmp = tmp->next;
	}
}
