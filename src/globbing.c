#include "../include/minishell.h"
#include <glob.h>
#include <stdio.h>

char	**readgloff(char **tab)
{
	char **glob;
	int y = 0;
	int x = 0;
	int t = 0;

	while (tab[y++]);
	if ((glob = malloc(sizeof(char *) * (y + 8))) == NULL)
		return (NULL);
	for (y = 0 ; tab[y] ; y++) {
		for (x = 0 ; tab[y][x] ; x++) {
			(tab[y][x] == '*' || tab[y][x] == '?'
			|| tab[y][x] == '[' || tab[y][x] == ']') ?
				glob[t++] = tab[y] : 0;
		}
	}
	glob[t] = NULL;
	return (glob);
}

char	**checkexeve(char **tab)
{
	int y = 0;
	int t = 0;
	char **path;

	while (tab[y++]);
	path = malloc(sizeof(char *) * (y + 8));
	for (y = 0 ; tab[y] ; y++)
		if (tab[y] && tab[y][0] == '-')
			path[t++] = tab[y];
	path[t] = NULL;
	return (path);
}

int	check_star(char *str)
{
	int i = 0;
	int nbr = 0;
	while (str[i]) {
		if (str[i] == '*' || str[i] == '?' ||
		str[i] == '[' || str[i] == ']')
			nbr++;
		i++;
	}
	if (nbr > 0)
		return (0);
	else
		return (1);
}

char	*globbing(char *str)
{
	unsigned int i = 0; glob_t globbuf; char **tab = str_to_tab(str, ' ');
	char **globt = NULL; char **exe = NULL; char *dest = tab[0];
	if (check_star(str))
		return (str);
	globt = readgloff(tab);
	exe = checkexeve(tab);
	if (glob(globt[0], 0, NULL, &globbuf) != 0) {
		printf("%s: No match.\n", tab[0]);
		return (NULL);
	}
	for (i = 0 ; globt[++i];)
		if (glob(globt[i],GLOB_APPEND,NULL,&globbuf) != 0) {
			printf("%s: No match.\n", tab[i]);
			return (NULL);
		}
	for (i = -1 ; exe[++i] ;)
		globbuf.gl_pathv[i] = exe[i];
	for (unsigned int a = 0 ; a <= globbuf.gl_pathc ; a++)
		dest = my_strcat(dest, my_strcat(" ", globbuf.gl_pathv[a]));
	return (dest);
}
