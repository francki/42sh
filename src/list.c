#include "../include/minishell.h"

void	append_string(list **head, char *str)
{
	list *elem = malloc(sizeof(list));
	list *last = *head;

	if (elem) {
		elem->str = my_strdup(str);
		elem->next = NULL;
		if (*head == NULL) {
			elem->prev = NULL;
			*head = elem;
			return;
		}
		while (last->next != NULL)
			last = last->next;
		elem->prev = last;
		last->next = elem;
	}
}

void	pop_node(list **head, list *del)
{
	if (!(*head) || !del)
		return;
	if (*head == del)
		*head = del->next;
	if (del->next != NULL)
		del->next->prev = del->prev;
	if (del->prev != NULL)
		del->prev->next = del->next;
	free(del);
	return;
}

void	print_list(list *head)
{
	while (head) {
		my_printf("[LIST]%s\n", head->str);
		head = head->next;
	}
}

void	free_list(list *src)
{
	if (src->next)
		free_list(src->next);
	free(src->str);
	free(src);
}
