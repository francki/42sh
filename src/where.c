#include "../include/minishell.h"

int	acces_where(list *s, char *str)
{
	int i = 0;
	while (s) {
		if (access(s->path, F_OK) == 0) {
			my_printf("%s\n", s->path);
			i = 1;
		}
		s = s->next;
	}
	if (i == 0) {
		my_printf("%s: Command not found.\n", str);
		return (1);
	}
	return (0);
}

int	check_builtins_where(char *str)
{
	(my_strcmp(str, "echo") == 0) ?
		my_printf("%s is a shell built-in\n",str): 0;
	(my_strcmp(str, "repeat") == 0) ?
		my_printf("%s is a shell built-in\n",str): 0;
	(my_strcmp(str, "setenv") == 0) ?
		my_printf("%s is a shell built-in\n",str): 0;
	(my_strcmp(str, "unsetenv") == 0) ?
		my_printf("%s is a shell built-in\n",str): 0;
	(my_strcmp(str, "cd") == 0) ?
		my_printf("%s is a shell built-in\n",str): 0;
	(my_strcmp(str, "where") == 0) ?
		my_printf("%s is a shell built-in\n",str): 0;
	(my_strcmp(str, "which") == 0) ?
		my_printf("%s is a shell built-in\n",str): 0;
	return (0);
}

int	where(char **argv, char **env, int *ret)
{
	int i = 1;
	list *path = get_path(tabdup(env));

	while (argv[i]) {
		path_concatener(path, argv[i]);
		check_builtins_where(argv[i]);
		*ret = acces_where(path, argv[i]);
		i++;
	}
	return (0);
}
