#include "my_printf.h"

void	my_putcharb(char c, int *total)
{
	write(1, &c, 1);
	*total += 1;
}
