#include "my_printf.h"

void	my_put_nbrb(int nb, int *total)
{
	int	modulo;
	int	div;

	if (nb < 0 && nb > -10)
		my_putcharb('-', total);
	modulo = nb % 10;
	div = nb / 10;
	if (nb >= 10 || nb <= -10)
		my_put_nbrb(div, total);
	if (modulo < 0)
		modulo = -modulo;
	my_putcharb(modulo + '0', total);
}
