#include "my_printf.h"

void	lu_print(va_list ap, int *total)
{
	my_put_ulnbr_it(va_arg(ap, unsigned long), total);
}

char	*my_epure_str(char *str, char c)
{
	int i = -1;
	int j = 0;
	char b = 1;
	char *ret = malloc(sizeof(char) * my_strlenb(str) + 1);

	if (ret)
		return (NULL);
	while (str[++i + j] && b) {
		if (str[i + j] == c) {
			ret[i] = str[i + j];
			i++;
			b = 0;
		}
		while (str[i + j] == c)
			j++;
		ret[i] = str[i + j];
	}
	ret[i] = '\0';
	return (ret);
}

int	as_a_flag(char *str)
{
	int i = 0;
	char b = 1;

	while (!IS_ALPHA(str[i]))
		i++;
	if (op_check_basic(str[i]) >= 0)
		return (1);
	if (oca(str[i], str[i + 1]) >= 0)
		return (2);
	return (0);
}
