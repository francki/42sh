#include "my_printf.h"

int	op_check_basic(char c)
{
	char *str = "dioxXucspbS%f";
	int i = -1;

	while (str[++i])
		if (str[i] == c)
			return (i);
	return (-1);
}

int	oca(char c1, char c2)
{
	char *str1 = "lL";
	char *str2 = "dixXu";
	int i = -1;
	int b = 0;

	while (str1[++i])
		if (str1[i] == c1)
			b = 1;
	i = -1;
	while (str2[++i])
		if (str2[i] == c2 && b)
			return (i);
	return (-1);
}

void	op_print_basic(va_list ap, int n, int *total)
{
	void (*ptrf[13])(va_list, int *);

	ptrf[0] = &d_i_print;
	ptrf[1] = &d_i_print;
	ptrf[2] = &o_print;
	ptrf[3] = &x_print;
	ptrf[4] = &bigx_print;
	ptrf[5] = &u_print;
	ptrf[6] = &c_print;
	ptrf[7] = &s_print;
	ptrf[8] = &p_print;
	ptrf[9] = &b_print;
	ptrf[11] = &modulo_print;
	ptrf[12] = &f_print;
	(*ptrf[n])(ap, total);
}

void	op_print_adv(va_list ap, int n, int *total)
{
	void (*ptrf[5])(va_list, int *);

	ptrf[0] = &ld_print;
	ptrf[1] = &ld_print;
	ptrf[2] = &lx_print;
	ptrf[3] = &lbigx_print;
	ptrf[4] = &lu_print;
	(*ptrf[n])(ap, total);
}

int	my_printf(char *str, ...)
{
	int i = -1;
	int total = 0;
	va_list ap;

	va_start(ap, str);
	while (str[++i]) {
		if (str[i] != '%')
			my_putcharb(str[i], &total);
		if (str[i] == '%' && op_check_basic(str[i + 1]) >= 0) {
			op_print_basic(ap, op_check_basic(str[i + 1]), &total);
			i++;
		}
		else if (str[i] == '%' && oca(str[i + 1], str[i + 2]) >= 0) {
			op_print_adv(ap, oca(str[i + 1], str[i + 2]), &total);
			i += 2;
		}
	}
	va_end(ap);
	return (total);
}
