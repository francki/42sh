#include "my_printf.h"

void	d_i_print(va_list ap, int *total)
{
	my_put_nbrb(va_arg(ap, int), total);
}

void	o_print(va_list ap, int *total)
{
	my_put_nbr_baseb(va_arg(ap, int), _base8, total);
}

void	x_print(va_list ap, int *total)
{
	my_put_nbr_baseb(va_arg(ap, int), _base16, total);
}

void	u_print(va_list ap, int *total)
{
	my_put_unbr(va_arg(ap, int), total);
}

void	c_print(va_list ap, int *total)
{
	my_putcharb((char)va_arg(ap, int), total);
}
