#include "my_printf.h"

int	my_put_lnbr_it(long nb, int *total)
{
	long div = 1;

	if (nb < 0) {
		write(1, "-", 1);
		*total += 1;
		nb = -nb;
	}
	while ((nb / div) >= 10)
		div *= 10;
	while (div >= 1) {
		my_putcharb((nb / div) + '0', total);
		nb %= div;
		div /= 10;
	}
	return (0);
}
