#ifndef __MY_PRINTF__
#define __MY_PRINTF__
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#define _base16 "0123456789abcdef"
#define _BASE16 "0123456789ABCDEF"
#define _base2 "01"
#define _base8 "01234567"
#define _base10 "0123456789"
#define IS_UPPER(c) ((c) >= 'A' && (c) <= 'Z')
#define IS_LOWER(c) ((c) >= 'a' && (c) <= 'z')
#define IS_ALPHA(c) (IS_UPPER(c) || IS_LOWER(c))

int	my_strlenb(char *str);
void	my_putstrb(char *str, int *total);
void	my_put_nbrb(int n, int *total);
void	my_put_float(double n, int *total);
void	my_put_lnbr(long n, int *total);
int	my_put_lnbr_it(long n, int *total);
int	my_put_ulnbr_it(unsigned long n, int *total);
void	my_put_unbr(unsigned int n, int *total);
void	my_put_nbr_baseb(unsigned int n, char *base, int *t);
void	my_put_lnbr_base(unsigned long n, char *base, int *t);
void	my_putcharb(char c, int *total);
char	*my_epure_str(char *str, char c);
int	as_a_flag(char *str);

void	d_i_print(va_list ap, int *total);
void	o_print(va_list ap, int *total);
void	x_print(va_list ap, int *total);
void	bigx_print(va_list ap, int *total);
void	u_print(va_list ap, int *total);
void	c_print(va_list ap, int *total);
void	s_print(va_list ap, int *total);
void	f_print(va_list ap, int *total);
void	p_print(va_list ap, int *total);
void	b_print(va_list ap, int *total);
void	modulo_print(va_list ap, int *total);
void	ld_print(va_list ap, int *total);
void	lu_print(va_list ap, int *total);
void	lx_print(va_list ap, int *total);
void	lbigx_print(va_list ap, int *total);

int	oca(char c1, char c2);
int	op_check_basic(char c);
void	op_printer_adv(va_list ap, int n, int *total);
void	op_printer_basic(va_list ap, int n, int *total);
int	my_printf(char *str, ...);

#endif /* __MY_PRINTF__ */
