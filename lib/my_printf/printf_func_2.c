#include "my_printf.h"

void	s_print(va_list ap, int *total)
{
	my_putstrb(va_arg(ap, char *), total);
}

void	p_print(va_list ap, int *total)
{
	write(1, "0x", 2);
	*total += 2;
	my_put_lnbr_base(va_arg(ap, long), _base16, total);
}

void	b_print(va_list ap, int *total)
{
	my_put_nbr_baseb(va_arg(ap, int), _base2, total);
}

void	modulo_print(va_list ap, int *total)
{
	write(1, "%", 1);
	*total += 1;
}
