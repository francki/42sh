#include "my_printf.h"

void	my_put_lnbr_base(unsigned long nb, char *base, int *t)
{
	if (!base)
		return;
	if (my_strlenb(base) == 10 && (long)nb < 0) {
		write(1, "-", 1);
		*t += 1;
		my_put_lnbr_base(-nb, base, t);
	}
	else {
		if (nb >= my_strlenb(base))
			my_put_lnbr_base(nb / my_strlenb(base), base, t);
		write(1, base + nb % my_strlenb(base), 1);
		*t += 1;
	}
}
