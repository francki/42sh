#include "my_printf.h"

void	my_put_float(double n, int *total)
{
	signed long int decipart;
	signed long int intpart;

	if (n < 0) {
		my_putcharb('-', total);
		n *= -1;
	}
	intpart = (long)n;
	my_put_nbrb(intpart, total);
	my_putcharb('.', total);
	n -= intpart;
	n *= 1000000;
	decipart = (long)(n + 0.5);
	my_put_nbrb(decipart, total);
}
