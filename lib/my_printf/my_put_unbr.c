#include "my_printf.h"

void	my_put_unbr(unsigned int nb, int *total)
{
	unsigned int	modulo;
	unsigned int	div;

	modulo = nb % 10;
	div = nb / 10;
	if (nb >= 10)
		my_put_nbrb(div, total);
	my_putcharb(modulo + '0', total);
}
