#include "my_printf.h"

void	bigx_print(va_list ap, int *total)
{
	my_put_nbr_baseb(va_arg(ap, int), _BASE16, total);
}

void	ld_print(va_list ap, int *total)
{
	my_put_lnbr_it(va_arg(ap, long), total);
}

void	lx_print(va_list ap, int *total)
{
	my_put_lnbr_base(va_arg(ap, long), _base16, total);
}

void	lbigx_print(va_list ap, int *total)
{
	my_put_lnbr_base(va_arg(ap, long), _BASE16, total);
}

void	f_print(va_list ap, int *total)
{
	my_put_float(va_arg(ap, double), total);
}
