#include "my_printf.h"

void	my_putstrb(char *str, int *total)
{
	write(1, str, my_strlenb(str));
	*total += my_strlenb(str);
}
