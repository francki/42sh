#include "my_printf.h"

int	my_put_ulnbr_it(unsigned long nb, int *total)
{
	long div = 1;

	while ((nb / div) >= 10)
		div *= 10;
	while (div >= 1) {
		my_putcharb((nb / div) + '0', total);
		nb %= div;
		div /= 10;
	}
	return (0);
}
