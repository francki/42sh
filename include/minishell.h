#ifndef _MINISHELL_H_
#define _MINISHELL_H_
#include "../include/my_printf.h"
#include "../include/gnl.h"
#define _GNU_SOURCE
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <dirent.h>
#include <sys/types.h>
#define MAX_PATH 256
#define UNUSED __attribute__((unused))

extern char **environ;
typedef struct list {
	struct list *next;
	struct list *prev;
	char *str;
	char *path;
}	list;

typedef struct old_path{
	char *old;
}	old_path;

typedef struct pipe {
	char *s;
	char **argv;
	char **env;
	list *t;
}	pipe_s;

int	where(char **argv, char **env, int *ret);
int	which(char **argv, char **env, int *ret);
char	*repeat_func(char **argv, char ***env, int *ret);
char	*trans_string(char *str);
char	*my_strcpy(char *dest, char *src);
int	is_viable(char **av);
void	echo_with(char *str);
void	echo_without(char *str);
void	echo(char **argv);
int	check_pipe(char *str);
char	*epure_red(char *str);
int	pipe_cmd(char *str, list *path, char ***env, int *ret);
char	*globbing(char *str);
int	cmpt_tab(char **tab);
int	redirection_dup(char *str);
char	*check_access(list *s, char *argv);
int	my_strlen(char *str);
list	*get_path(char **env);
void	path_concatener(list *paths, char *str);
char	*my_epure_strr(char *str);
int	execute(char *s, char **argv, char **env, char *str);
int	hub(char **argv, char ***env, int *ret);
void	append_string(list **head, char *str);
void	print_list(list *head);
void	free_list(list *l);
int	launch(char *str, list *path, char ***env);
int	minishell(char **env);
int	char_oc(char *str, char c);
char	**str_to_tab(char *str, char c);
char	*my_strcat(char *buf, char *str);
char	*my_strdup(char *str);
int	my_strncmp(char *str1, char *str2, int n);
int	my_strcmp(char *str1, char *str2);
void	str_to_wordlist(char *str, list **head, char c);
void	tab_to_list(char **str, list **head);
void	handling_c(int signum);
void	env_print(char **env);
void	setenv_func(char **argv, char ***env);
void	unsetenv_func(char **argv, char ***env);
void	cd_func(char **argv, char ***env, int *ret);
int	my_tablen(char **str);
int	setenv_error_check(char **argv, char **env);
int	unsetenv_error_check(char **argv, char **env);
char    **my_setenv(char **argv, char **env);
void	error_hl(int status);
int	error_exe(char *argv);
int	cd_minus(void);
int	cd_classic(char ***env, char *pwd, char *str, char *first);
void	cd_home(char ***env, char *pwd, char *first);
char	*env_search(char *str, int n, char ***env);
char	*get_pwd(void);
char	**argv_maker(char *str, char *str1, char *str2);
void	free_tab(char **tab);
int	is_semicolon(char *str);
void	parse_and_x(char *str, char ***env, int *ret);
int	return_selector(int status);
int     unsetenv_err(char **argv);
int     in_env(char **argv, char *token);
char	**tabdup(char **tab);
char    **make_unsetenv_func(char **argv, char ***env);
pipe_s    init_s(char *s, char **argv, char **env, list *tmp);
int	opener_pipe(char sw);
int	dupper(pipe_s s, int *ex, char *swap);
int     execute_pipe(pipe_s s, char *swap);
int	launch_pipe(list *tmp, list *path, char ***env, char *sw);
char	*my_itoa(int nb);

#endif
