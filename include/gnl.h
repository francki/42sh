#ifndef	__LIBGNL__
#define	__LIBGNL__

char	*get_next_line(int fd);
char	*my_gnl(const int fd);
char	*get_next_line_dbg(int fd);

#endif
